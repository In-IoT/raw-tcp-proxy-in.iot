package net.boboman13.raw_tcp_proxy;

public class ForwardAddressAndPort {
	
	private String address;
	
	private int port;

	
	
	public ForwardAddressAndPort(String address, int port) {
		super();
		this.address = address;
		this.port = port;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
	
	

}
