package net.boboman13.raw_tcp_proxy.main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import net.boboman13.raw_tcp_proxy.ConfigClass;
import net.boboman13.raw_tcp_proxy.EurekaMethods;
//import net.boboman13.raw_tcp_proxy.EurekaMethods;
import net.boboman13.raw_tcp_proxy.Proxy;
import net.boboman13.raw_tcp_proxy.Config.FileResourceLoader;
import net.boboman13.raw_tcp_proxy.Config.IConfig;
import net.boboman13.raw_tcp_proxy.Config.IResourceLoader;
import net.boboman13.raw_tcp_proxy.Config.ResourceLoaderConfig;

/**
 * @author boboman13
 */

public class RunTCPProxy {

	public static void main(String[] args) {
		
		checkIfConfigFileExists();
		File defaultConfigurationFile = defaultConfigFile();
        IResourceLoader filesystemLoader = new FileResourceLoader(defaultConfigurationFile);
		
		final IConfig config = new ResourceLoaderConfig(filesystemLoader);
		
		
		String out = "127.0.0.1";
		String listenIP = "0.0.0.0";
		int port = 5683;
		int listenPort = Integer.parseInt(config.getProperty("listenPort"));
		boolean debug = false;
		
		ConfigClass.eurekaAddress = config.getProperty("eurekaAddress");
		ConfigClass.eurekaUsername = config.getProperty("eurekaUsername");
		ConfigClass.eurekaPassword = config.getProperty("eurekaPassword");

		// Parse through arguments.
		for (int i = 0; i < args.length; i++) {
			// Look for out (-o, --out)
			if (args[i].equalsIgnoreCase("-o") || args[i].equalsIgnoreCase("--out")) {
				out = args[i + 1];
			}
			// Look for port (-p, --port)
			if (args[i].equalsIgnoreCase("-p") || args[i].equalsIgnoreCase("--port")) {
				port = Integer.parseInt(args[i + 1]);
			}
			// Look for listening port (-l, --listen)
			if (args[i].equalsIgnoreCase("-l") || args[i].equalsIgnoreCase("--listen")) {
				listenPort = Integer.parseInt(args[i + 1]);
			}
			// look for host IP (-h, --host)
			if (args[i].equalsIgnoreCase("-h") || args[i].equalsIgnoreCase("--host")) {
				listenIP = args[i + 1];
			}
			// Look for debug (-d, --debug)
			if (args[i].equalsIgnoreCase("-d") || args[i].equalsIgnoreCase("--debug")) {
				debug = true;
				System.out.println("Debug mode activated.");
			}
		}
		
		// Initiate the Proxy
		Proxy proxy = new Proxy();
		proxy.setHost(out);
		proxy.setPort(port);
		proxy.setListeningIP(listenIP);
		proxy.setListeningPort(listenPort);
		proxy.setDebug(debug);

		new Thread(new Runnable() {

			public void run() {

				while (true) {
					try {
						
						EurekaMethods eurekaMethods = new EurekaMethods();
						eurekaMethods.getMQTTInstances();
						Thread.sleep(50000);
						
						}

						catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}}
		).start();
		
//		ArrayList<ForwardAddressAndPort> forwardAddressAndPorts;
//		forwardAddressAndPorts = new ArrayList<>();
//		
//		ForwardAddressAndPort forwardAddressAndPort = new ForwardAddressAndPort("192.168.60.116", 5683);
//		forwardAddressAndPorts.add(forwardAddressAndPort);
//		ConfigClass.forwardAddressAndPort = new ArrayList<>(forwardAddressAndPorts);

		// Start the proxy.
		proxy.start();
		System.out.println("Port is running!!!!! " +config.getProperty("listenPort"));
	}
	
	
	private static File defaultConfigFile() {
		
		String configPath = System.getProperty("user.dir");
        //String configPath = System.getProperty("moquette.path", null);
        System.out.println("This is the configPath " + configPath);
        return new File(configPath, IConfig.DEFAULT_CONFIG);
    }
	
	private static void checkIfConfigFileExists() {

		String filename = System.getProperty("user.dir")+ File.separator + "config" + File.separator + "proxy.conf";
		File file = new File(filename);
		if (!file.exists()) {
			file.mkdirs();
			file.delete();
			try {
				file.createNewFile();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try (Writer writer = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream(filename), "utf-8"))) {
				writer.write("listenPort 5000\n" + 
						"\n" + 
						"eurekaAddress http://localhost:8761\n" + 
						"\n" + 
						"eurekaUsername inIoTTest\n" + 
						"eurekaPassword lucasabbadeWare");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
