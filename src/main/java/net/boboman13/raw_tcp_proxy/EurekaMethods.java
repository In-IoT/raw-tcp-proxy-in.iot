package net.boboman13.raw_tcp_proxy;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public class EurekaMethods {

	public void getMQTTInstances() {

		CloseableHttpClient client;
		ArrayList<ForwardAddressAndPort> forwardAddressAndPorts;
		forwardAddressAndPorts = new ArrayList<>();

		client = HttpClients.createDefault();

		HttpGet httpGet = new HttpGet(ConfigClass.eurekaAddress+"/eureka/apps/mqtt-instances");
		httpGet.setHeader("Accept", "application/json");
		

		String auth = ConfigClass.eurekaUsername + ":" + ConfigClass.eurekaPassword; //"inIoTTest:lucasabbadeWare";
		byte[] encodedAuth = Base64.encodeBase64(
		  auth.getBytes(StandardCharsets.ISO_8859_1));
		String authHeader = "Basic " + new String(encodedAuth);
		httpGet.setHeader(HttpHeaders.AUTHORIZATION, authHeader);

		HttpResponse httpResponse = null;

		try {
			httpResponse = client.execute(httpGet);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(httpResponse == null)
			return;
		
		if(httpResponse.getStatusLine().getStatusCode() == 200) {

		HttpEntity entity = httpResponse.getEntity();
		String responseString = null;
		try {
			responseString = EntityUtils.toString(entity, "UTF-8");
		} catch (ParseException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// System.out.println(responseString);

		String str = "{ \"instance\":"
				+ responseString.substring(responseString.indexOf("["), responseString.lastIndexOf("]"))
				+ "]}";
		JSONObject jsonObject = new JSONObject(str);

		JSONArray lineItems = jsonObject.getJSONArray("instance");
		for (Object o : lineItems) {
			JSONObject jsonLineItem = (JSONObject) o;
			String address = jsonLineItem.getString("ipAddr");
			JSONObject jsonObjectPort = jsonLineItem.getJSONObject("port");

			int port = jsonObjectPort.getInt("$");

			ForwardAddressAndPort forwardAddressAndPort = new ForwardAddressAndPort(address, port);
			forwardAddressAndPorts.add(forwardAddressAndPort);
		}

		try {
			client.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ConfigClass.forwardAddressAndPort = new ArrayList<>(forwardAddressAndPorts);
		
		}

	}

}
